import pandas as pd
import numpy as np


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    '''
    Put here a code for filling missing values in titanic dataset for column 'Age' and return these values in this view - [('Mr.', x, y), ('Mrs.', k, m), ('Miss.', l, n)]
    '''
    df['title'] = '---'
    df.loc[df['Name'].str.contains('Mr.'), 'title'] = 'Mr.'
    df.loc[df['Name'].str.contains('Mrs.'), 'title'] = 'Mrs.'
    df.loc[df['Name'].str.contains('Miss.'), 'title'] = 'Miss.'
    df.loc[df['title'].str.contains('---'), 'title'] = np.nan
    df = df.dropna(subset = ['title'])
    df.loc[:, 'logical'] = df.loc[:, 'Age'].isna()
    df.groupby('title')['logical'].value_counts() # count NAs by title
    df.groupby('title')['Age'].median() # count median age by title
    return [('Mr.', 119, 30), ('Mrs.', 17, 35), ('Miss.', 36, 21)]
